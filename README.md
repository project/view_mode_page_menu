INTRODUCTION
------------

The View Mode Page Menu module works with the [View Mode Page](https://www.drupal.org/project/view_mode_page) module to create a dynamic menu block that shows related links for the given node.

REQUIREMENTS
------------

This module requires the following modules:

 * View Mode Page (https://www.drupal.org/project/view_mode_page)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

 * Navigate to Administration » Structure » Block Layout:

    * Click `Place Block` on a region were you want the View Mode Page Menu block to be shown.

    * Find the `View Mode Page Menu` block within the list. Click `Place Block` to add the block to the region.

    * Configure the `View Mode Page Menu` block based on your requirements. Click `Save block`.
