<?php

declare(strict_types=1);

namespace Drupal\view_mode_page_menu\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\Annotation\Block;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Form\FormStateInterface;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\Core\Annotation\ContextDefinition;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Define the view mode page menu block.
 *
 * @Block(
 *   id = "view_page_mode_menu",
 *   admin_label = @Translation("View Page Mode Menu"),
 *   context_definitions = {
 *     "node" = @ContextDefinition("entity:node", label = @Translation("Node"))
 *   }
 * )
 */
class ViewModePageMenu extends BlockBase implements ContainerFactoryPluginInterface {

  /**
   * @var \Symfony\Component\HttpFoundation\Request|null
   */
  protected $request;

  /**
   * @var \Drupal\path_alias\PathAliasInterface
   */
  protected $pathAliasManager;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * {@inheritDoc}
   */
  protected function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    RequestStack $request_stack,
    AliasManagerInterface $path_alias,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    parent::__construct(
      $configuration,
      $plugin_id,
      $plugin_definition
    );
    $this->request = $request_stack->getCurrentRequest();
    $this->pathAliasManager = $path_alias;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    return new self(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('request_stack'),
      $container->get('path_alias.manager'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function defaultConfiguration() {
    return [
      'show_parent' => FALSE
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritDoc}
   */
  public function blockForm($form, FormStateInterface $form_state): array {
    $form = parent::blockForm($form, $form_state);

    $form['show_parent'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show Parent'),
      '#description' => $this->t('If checked, the parent link will be appended
        to the menu links'),
      '#default_value' => $this->showParent()
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->setConfiguration(
      $form_state->cleanValues()->getValues()
    );
  }

  /**
   * {@inheritDoc}
   */
  public function build() {
    return [
      '#theme' => 'view_mode_page_menu',
      '#links' => $this->getCurrentNodeMenusLinks(),
      '#context' => $this->getContextValue('node'),
    ];
  }

  /**
   * Build view mode page active menu links.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The content entity instance.
   *
   * @return array
   *   An array of the active menu links.
   */
  protected function buildViewModePageActiveMenuLinks(
    ContentEntityInterface $entity
  ): array {
    $links = [];

    try {
      if ($internal = $entity->toUrl('canonical')->getInternalPath()) {
        $alias_path = $this->pathAliasManager->getAliasByPath("/{$internal}");

        if ($entity->getEntityType()->hasLinkTemplate('canonical')) {
          $request_path = $this->request->getPathInfo();

          /** @var \Drupal\view_mode_page\Entity\ViewmodepagePattern $pattern */
          foreach ($this->getViewModePageEntityPatterns($entity) as $pattern) {
            if (!$pattern->applies($entity)) {
              continue;
            }
            $weight = $this->determineLinkOrderSequence(
              $links,
              (int) $pattern->getWeight()
            );
            $path = str_replace('%', $alias_path, $pattern->getPattern());

            $links[$weight] = [
              'path' => $path,
              'title' => $pattern->label(),
              'active' => $path === $request_path,
              'parent' => FALSE,
            ];
          }

          if ($this->showParent()) {
            array_unshift($links, [
              'path' => $alias_path,
              'title' => $entity->label(),
              'active' => $alias_path === $request_path,
              'parent' => TRUE,
            ]);
          }
        }
      }
    } catch (\Exception $exception) {
      watchdog_exception('view_mode_page_menu', $exception);
    }
    ksort($links, SORT_NUMERIC);

    return $links;
  }

  /**
   * Determine if parent menu links should be shown.
   *
   * @return bool
   *   Return TRUE to show parent menu link; otherwise FALSE.
   */
  protected function showParent(): bool {
    return (bool) $this->getConfiguration()['show_parent'] ?? FALSE;
  }

  /**
   * Determine menu link order sequence.
   *
   * @param array $links
   *   An array of menu links.
   * @param int $weight
   *   The link weight value.
   *
   * @return int
   *   The link order sequence integer.
   */
  protected function determineLinkOrderSequence(
    array $links,
    int $weight
  ): int {
    if (!isset($links[$weight])) {
      return (int) $weight;
    }

    return $this->determineLinkOrderSequence($links, $weight + 1);
  }

  /**
   * Get the relevant node view mode page links.
   *
   * @return array
   *   An array of menu links for the given node context.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  protected function getCurrentNodeMenusLinks(): array {
    return $this->buildViewModePageActiveMenuLinks(
      $this->getContextValue('node')
    );
  }

  /**
   * Get teh view mode page entity patterns.
   *
   * @param \Drupal\Core\Entity\ContentEntityInterface $entity
   *   The entity instance.
   *
   * @return array
   *   An array of the view mode page patterns.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getViewModePageEntityPatterns(
    ContentEntityInterface $entity
  ): array {
    return $this->getViewModePageStorage()->loadByProperties([
      'type' => "canonical_entities:{$entity->getEntityTypeId()}",
    ]);
  }

  /**
   * Get the view mode page pattern storage.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   The entity storage instance.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getViewModePageStorage(): EntityStorageInterface {
    return $this->entityTypeManager
      ->getStorage('view_mode_page_pattern');
  }
}
